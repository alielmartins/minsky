#ifndef MINSKY_H_INCLUDED
#define MINSKY_H_INCLUDED

class Minsky{
private:

    int flag_on=false;
    int temperatura0=200;
    int distancia0=300;
    int battery=0;
    int upboard=1;
public:
    void home();
    bool starter(int auth);
    int get_sensors(int id_port);
    // STATUS GETS
    int get_arduino_status();
    int get_battery_status();
    int get_upboard_status();
    int get_motorg_status();
    int get_motore_status();
    int get_motorfd_status();
    int get_motorfe_status();
    int get_motortd_status();
    int get_motorte_status();
    // --------------
    
    int get_temperatura0();
    int get_distancia0();
    int wait_key(int caracter);
    int def_var(char * texto);

};


#endif // MINSKY_H_INCLUDED

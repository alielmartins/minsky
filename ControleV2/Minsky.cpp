#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <string.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <Minsky.h>
#include <serialport.h>


void Minsky::home(){
    return;
}

bool Minsky::starter(int auth){
    char dev_name[25] = "\0";
    int id_port=0;
    printf("    [Ok]Conectando-se ao Minsky \n");
    printf("        Procurando dispositivos \n");
    for(int x=0;x < 2;x++){
        sprintf(dev_name,"/dev/ttyAMA%d",x);
        serialport MinhaPorta(dev_name,false);
        if(MinhaPorta.get_arduinoflag()){
            //printf("Conectado \n");
            flag_on = MinhaPorta.get_arduinoflag();
            id_port =  MinhaPorta.get_portid();
            break;
        }else{
            //printf("Desconectado \n");
        }
    }
    if(flag_on){
        get_sensors(id_port);
    }
    return flag_on;
}

int Minsky::get_sensors(int id_port){
    int caracteres=0,bytes=0,contador=0;
    char sensor_string[256] = "\0";
    if(flag_on){
        printf("Capturando valores dos sensores na porta serial \n");
        while(true){
            fflush(stdin);
            bytes = read(id_port,(void*)&caracteres,1);
            if(bytes > 0){
                if(wait_key(caracteres)){          // Primeira barra detectada
                    while(true){                    // Começa gravar os bytes da serial
                        fflush(stdin);
                        caracteres = 0;
                        bytes = read(id_port,(void*)&caracteres,1);
                        if(bytes > 0){
                            if(wait_key(caracteres)){  // Segunda barra detectada
                                break;
                            }
                            sensor_string[contador] = (char)caracteres;
                           //printf("%d \n",caracteres);
                            contador++;
                        }
                        //printf("#@FINAL@#");
                    }
                    def_var(sensor_string);  // Set a variavel do sensor recebido
                }
            }    
            contador=0;
            memset(&sensor_string,0,256);
            caracteres = 0;
            bytes = 0;
        }
    }
}

int Minsky::wait_key(int caracter){
    if(caracter == 124){ // | Na tabela ASCII
        return true;
    }
    return false;
}
int Minsky::def_var(char * texto){
    // Função responsavel por capturar os sensores e definir os valores das variaveis
    printf("Sensor: %s : Tamanho: %d\n",texto,strlen(texto));
    if(strncmp(texto,"T0:",3) == 0){
        printf("Sensor de Temperatura0 encontrado. \n");
        temperatura0 = atoi(&texto[3]);
        printf("Temperatura: %d \n",temperatura0);
    }
    
    if(strncmp(texto,"D0:",3) == 0){
        printf("Sensor de Distancia0 encontrado. \n");
        distancia0 = atoi(&texto[3]);
        printf("Distancia: %d \n",get_distancia0());
    }
    if(strncmp(texto,"D1:",3) == 0){
        printf("Sensor de Distancia0 encontrado. \n");
        distancia1 = atoi(&texto[3]);
        printf("Distancia: %d \n",get_distancia1());
    }
    if(strncmp(texto,"D2:",3) == 0){
        printf("Sensor de Distancia0 encontrado. \n");
        distancia2 = atoi(&texto[3]);
        printf("Distancia: %d \n",get_distancia2());
    }
    if(strncmp(texto,"D3:",3) == 0){
        printf("Sensor de Distancia0 encontrado. \n");
        distancia3 = atoi(&texto[3]);
        printf("Distancia: %d \n",get_distancia3());
    }
    if(strncmp(texto,"D4:",3) == 0){
        printf("Sensor de Distancia0 encontrado. \n");
        distancia4 = atoi(&texto[3]);
        printf("Distancia: %d \n",get_distancia4());
    }
    if(strncmp(texto,"D5:",3) == 0){
        printf("Sensor de Distancia0 encontrado. \n");
        distancia5 = atoi(&texto[3]);
        printf("Distancia: %d \n",get_distancia5());
    }
    if(strncmp(texto,"D6:",3) == 0){
        printf("Sensor de Distancia0 encontrado. \n");
        distancia6 = atoi(&texto[3]);
        printf("Distancia: %d \n",get_distancia6());
    }
    if(strncmp(texto,"D7:",3) == 0){
        printf("Sensor de Distancia0 encontrado. \n");
        distancia7 = atoi(&texto[3]);
        printf("Distancia: %d \n",get_distancia7());
    }
    if(strncmp(texto,"D8:",3) == 0){
        printf("Sensor de Distancia0 encontrado. \n");
        distancia8 = atoi(&texto[3]);
        printf("Distancia: %d \n",get_distancia8());
    }

    if(strncmp(texto,"B0:",3) == 0){
        printf("Status da bateria\n");
        battery = atoi(&texto[3]);
        printf("Bateria: %d \n",get_battery_status());
    }

    if(strncmp(texto,"C0:",3) == 0){
        printf("Status de cores 1\n");
        color1 = atoi(&texto[3]);
        printf("Cor 1: %d \n",get_cor1());
    }
    if(strncmp(texto,"C1:",3) == 0){
        printf("Status de cores 2\n");
        color2 = atoi(&texto[3]);
        printf("Cor 2: %d \n",get_cor2());
    }
    if(strncmp(texto,"M0:",3) == 0){
        printf("Marcha do Minsky\n");
        marcha = atoi(&texto[3]);
        printf("Marcha: %d \n",get_marcha());
    }
}
// GETS SENSORES
int Minsky::get_temperatura0(){
    return temperatura0;
}
int Minsky::get_distancia0(){
    return distancia0;
}
int Minsky::get_distancia1(){
    return distancia1;
}

int Minsky::get_distancia2(){
    return distancia2;
}
int Minsky::get_distancia3(){
    return distancia3;
}
int Minsky::get_distancia4(){
    return distancia4;
}
int Minsky::get_distancia5(){
    return distancia5;
}
int Minsky::get_distancia6(){
    return distancia6;
}
int Minsky::get_distancia7(){
    return distancia7;
}
int Minsky::get_distancia8(){
    return distancia8;
}

// GETS STATUS
int Minsky::get_arduino_status(){
    return flag_on;
}
int Minsky::get_upboard_status(){
    return upboard;
}

int Minsky::get_cor1(){
    return color1;
}
int Minsky::get_cor2(){
    return color2;
}
int Minsky::get_marcha(){
    return marcha;
}

int Minsky::get_motore_status(){
    return 1;
}
int Minsky::get_motorg_status(){
    return 1;
}
int Minsky::get_motorfd_status(){
    return 1;
}
int Minsky::get_motorfe_status(){
    return 1;
}
int Minsky::get_motortd_status(){
    return 1;
}
int Minsky::get_motorte_status(){
    return 1;
}
int Minsky::get_battery_status(){
    return battery;
}
// ---------------



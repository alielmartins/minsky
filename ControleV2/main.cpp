#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h> // sockaddr_in
#include <arpa/inet.h>
#include <jsoncpp/json/json.h>
//#include <librealsense/rs.hpp>
#include <ModuloVideo.h>
#include <serialport.h>
#include <Minsky.h>
#include <jpeglib.h>
    
#define server_agent "Minsky WebService 2.0v"
#define port 8080
#define pedidos 65565
#define packet_size 1024*10
#define camera_nome "Intel RealSense F200"
#define auth_arduino true
#define Camera_Atual 2

struct clients_infos{									// Struct necessaria para passar informações para thread
    long socket_id;
    pthread_t threads_id;
    struct sockaddr settings;
};

void * Start_Server(void * params);
long firewall(void * infos);
void * clients(void * params);
long verificar_pacotes(char * buffer,size_t tamanho);
char * tratar_http_get(char * buffer,int nbytes,int * ztamanho);
char * carregar_arquivo(char * name,size_t * zfile);
char * tratar_jsonrpc(char * buffer,size_t tam,size_t * tam_out);
char * tratar_streaming(int socket,char * buffer,size_t tam,size_t * tam_out);
char * tratar_imagen(char * buffer,size_t tam,size_t * tam_out);
char * jsonrpc_res(char * buffer,size_t tam,size_t * tam_out);
bool cameras();

Minsky objMinsky;
Streaming RealCamera;

int main(){
    pthread_t threads;
    printf("[*] Controle %s\n",server_agent);
    pthread_create(&threads,NULL,&Start_Server,NULL);
    printf("    [!] Inicializando Servidor\n");
    if(threads != 0){
        printf("        Start [OK]\n        IP: 0.0.0.0\n        Port: %d\n",port);
    }
    printf("    [!] Procurando RealSense \n");
    if(!cameras()){
        
    }
    objMinsky.starter(auth_arduino);
    while(1){
        sleep(100);
    }
    return 0;
}
void * Start_Server(void * params){
    long sockets=0,binds=0,clients_socket=0;
    socklen_t clients_size=0;
    pthread_t threads=0;
    struct sockaddr_in settings;
    struct sockaddr_in clients_settings;
    struct clients_infos infos_clients;
    long tentativas = 0;
    long tconnections=0;
    dnv:sockets = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
    if(sockets == -1){
        printf("    [F] Não foi possivel definir o servidor \n");
        close(sockets);
        pthread_exit(0);
    }else{
        printf("    [Ok] Socket definido com sucesso. \n");
    }
    memset(&settings,0,sizeof(settings));
    settings.sin_family = AF_INET;
    settings.sin_addr.s_addr = INADDR_ANY;
    settings.sin_port = htons(port);
    if( (binds == bind(sockets,(struct sockaddr*)&settings,sizeof(settings))) <= 0){
        printf("    %d - [F] Não foi possivel inicializar na porta %d \n",tentativas,port);
        close(sockets);
        tentativas++;
        if(tentativas > 100){
            pthread_exit(0);
        }
        sleep(5);
        goto dnv;
    }else{
        printf("    [Ok] Bind inicializado com sucesso \n");
    }
    listen(sockets,pedidos);
    printf("    [Ok] Aguardando Conexão \n");
    clients_size = sizeof(struct sockaddr_in);
    while(true){
        clients_socket = accept(sockets,(struct sockaddr*)&clients_settings,&clients_size);
        if(clients_socket == -1){
            printf("[F] Falha em estabelecer conexão com cliente \n");
        }else{
            printf("    [Ok] %d Cliente %s:%d conectado com o servidor. \n",tconnections,inet_ntoa(clients_settings.sin_addr),port);
            tconnections++;
            infos_clients.socket_id = clients_socket;
            memcpy(&infos_clients.settings,&clients_settings,sizeof(clients_settings));
            if(firewall(&infos_clients)){
                    pthread_create(&threads,NULL,&clients,&infos_clients);
                    infos_clients.threads_id = threads;
                    printf("1- Thread ID: %p \n",infos_clients.threads_id);
                    //pthread_join(threads,NULL);
                }else{
                    printf("[F] Cliente %s não autorizado\nDesconectado!!\n",inet_ntoa(clients_settings.sin_addr));
                    close(clients_socket);
            }
        }
    }
    close(sockets);
    pthread_exit(0);
}

long  firewall(void * infos){															// Verifica se o cliente pode entrar na rede
	return 1;
}

void * clients(void * params){                                                          // Função que executa funções para o cliente
    struct clients_infos infos_clientes;
    char * buffer = NULL;
    char * buffer_out = NULL;
    char * tmp = NULL;
    long v_pedidos = 0;
    long nbytes =0,zbuffer_out=0;
    memset(&infos_clientes,0,sizeof(infos_clientes));
    memcpy(&infos_clientes,params,sizeof(infos_clientes));                              // Recupera os dados do cliente
    buffer = (char*)malloc(packet_size);
    if(buffer == NULL){
        printf("    [F] Falha em alocar espaço  \n");
        close(infos_clientes.socket_id);
        pthread_exit(0);
    }
    printf("2- Thread ID: %p \n",infos_clientes.threads_id);
    printf("    Aguardando dados do cliente \n");
    memset(buffer,0,sizeof(packet_size));
    if( (nbytes = recv(infos_clientes.socket_id,buffer,packet_size,0)) <  0){
        printf("    [F] Falha em receber reposta do cliente \n");
        printf("    [F] Cliente Socket %d \n",infos_clientes.socket_id);
        close(infos_clientes.socket_id);
        pthread_exit(0);
    }
    if(nbytes <= 0 ){
        close(infos_clientes.socket_id);
        pthread_exit(0);
    }
    printf("        Cliente Socket: %d \n        Bytes: %d \n",infos_clientes.socket_id,nbytes);
    v_pedidos = verificar_pacotes(buffer,nbytes);
    if(v_pedidos > 0){
        printf("    [Ok] Pacote aceito pelo servidor \n");
        if(v_pedidos == 1){
            buffer_out = tratar_http_get(buffer,nbytes,(int *)&zbuffer_out);
        }
        if(v_pedidos == 2){
            buffer_out = tratar_streaming(infos_clientes.socket_id,buffer,nbytes,(size_t*)&zbuffer_out);
        }
        if(v_pedidos == 3){
            tmp = tratar_jsonrpc(buffer,nbytes,(size_t*)&zbuffer_out);
            buffer_out = jsonrpc_res(tmp,zbuffer_out,(size_t*)&zbuffer_out);
        }
        if(v_pedidos == 4){
            buffer_out = tratar_imagen(buffer,nbytes,(size_t*)&zbuffer_out);
        }
        if(nbytes == send(infos_clientes.socket_id,buffer_out,zbuffer_out,0)==-1){
            printf("    [F] Falha em retornar pacote \n");
        }else{
            printf("    [OK] Pointer: %p\n    Tamanho: %d \n",buffer_out,zbuffer_out);
            printf("    [Ok] Resposta devolvida para o cliente \n");
        }  
    }else{
            printf("    [F] Pacote não aceito pelo servidor \n");
    }
    close(infos_clientes.socket_id);
    free(buffer);    
    free(buffer_out);                                                
    pthread_exit(0);
    pthread_detach(infos_clientes.threads_id);
}

long verificar_pacotes(char * buffer,size_t tamanho){
    char r_pedidos[256] = "\0",*p_pedido;
    printf("        [*] Analise de pacote \n");
    if(strncmp(buffer,"GET /",5)==0){
        printf("        Tipo: HTTP-GET \n");        
        strncpy(r_pedidos,&buffer[4],25);
        p_pedido = strtok(r_pedidos," ");
        if(strncmp(p_pedido,"/streaming",10) == 0){
                printf("        Tipo: Streaming \n");
            return 2;
        }
        if(strncmp(p_pedido,"/images",7) == 0){
                printf("        TIPO: IMAGEMM! \n");
                return 4;
            }
        return 1;
    }
    if(strncmp(buffer,"POST /",6) == 0){
    strncpy(r_pedidos,&buffer[5],25);
    p_pedido = strtok(r_pedidos," ");
        if(strncmp(p_pedido,"/jsonrpc",8)==0){
            printf("        Tipo: HTTP-POST-JSON \n");
            return 3;
        }
    }
    return 0;
}   

char * tratar_http_get(char * buffer,int nbytes,int * ztamanho){
    printf("        [Ok] Tratamento de HTTP \n");
    char pagina[25] = "\0";
    char r_pagina[25] = "\0";
    char pacote_header[256] = "\0";
    char * saida = NULL;
    char * barquivo = NULL;
    strncpy(pagina,&buffer[4],25);
    strtok(pagina," ");
    printf("          Pagina:  %s \n",pagina);
    if(strcmp(pagina,"/")==0){
        printf("          [!!] Pagina raiz solicitada \n");
        barquivo = carregar_arquivo((char*)"www/index.html",(size_t*)ztamanho);
    }else{
        sprintf(r_pagina,"www%s",pagina);
        barquivo = carregar_arquivo(r_pagina,(size_t*)ztamanho);
    }
    if(barquivo == NULL){
        printf("    [F] Retornar 404\n");
        barquivo = carregar_arquivo((char*)"www/404.html",(size_t*)ztamanho);
    }
    sprintf(pacote_header,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\nContent-Type: text/html; charset=UTF-8\n\n",server_agent);
    saida = (char*)malloc(strlen(pacote_header)+*ztamanho);
    memcpy(saida,&pacote_header[0],strlen(pacote_header));
    memcpy(&saida[strlen(pacote_header)],barquivo,*ztamanho);
    free(barquivo);
    return saida;
}

char * carregar_arquivo(char * name,size_t * zfile){
    FILE * wFile;
    char * buffer = NULL;
    int nbytes=0;
    wFile = fopen(name,"rb");
    if(wFile == NULL){
        printf("    [F] Nao foi possivel encontrar o arquivo %s\n",name);
        return NULL;
    }
    fseek(wFile,0,SEEK_END);
    *zfile = ftell(wFile);
    rewind(wFile);
    buffer = (char*)malloc(*zfile);
    memset(buffer,0,sizeof(*zfile));
    nbytes = fread(&buffer[0],*zfile,1,wFile);
    if(nbytes < 0){
        printf("    [F] Falha na leitura dos bytes do arquivo solicitado. \n");
        fclose(wFile);
        return NULL;
    }
    fclose(wFile);
    return buffer;
}

char * tratar_jsonrpc(char * buffer,size_t tam,size_t * tam_out){
    printf("    [Ok] Tratamento de JSONRPC \n");
    // Extrair o JSONRPC do HTTP e retorna um ponteiro para tal
    char * buffer_cpy = NULL,*jsonrpc_data=NULL,*jsonrpc_zdata=NULL,*tmp=NULL;
    char njson[256] = "\0";
    int zjson = 0;
    buffer_cpy = (char*)malloc(tam);
    memset(buffer_cpy,0,tam);
    memcpy(buffer_cpy,buffer,tam);
    jsonrpc_zdata = strstr(buffer_cpy,"Content-Length:");
    jsonrpc_zdata = strtok(jsonrpc_zdata,"\n");
    zjson = atoi(&jsonrpc_zdata[16]);
    // -------------------------
    tmp = strstr(buffer,"{");
    jsonrpc_data = (char*)malloc(zjson);
    memcpy(jsonrpc_data,tmp,zjson);
    Json::Reader reader;
    Json::Value obj;
    reader.parse(jsonrpc_data,obj);
    printf("    Versão: %s\n",obj["JSONRPC"].asCString());
    printf("    Method: %s\n",obj["method"].asCString());
    printf("    Params: %s\n",obj["params"].asCString());
    printf("    ID: %d\n",obj["ID"].asInt());
    free(buffer_cpy);
    *tam_out = zjson;
    // ---------------------------------------------------------
    return jsonrpc_data;
}

char * jsonrpc_res(char * buffer,size_t tam,size_t * tam_out){
    char json_data[256]= "\0";
    char * pjson_data = NULL;
    Json::Reader reader;
    Json::Value obj;
    //printf("    [#] Tratamento de JSON--JSONRES\n");
    //printf("     [#] Conteudo: %s \n",buffer);
    //printf("     [#] Tamanho: %d \n",tam);
    reader.parse(buffer,obj);
    //printf("      [#] ID-RES: %d \n",obj["id"].asInt());
    //printf("      [#] Meth-RES: %s \n",obj["params"].asCString());
    // RECONHECE O PEDIDO DE SENSOR, REESCREVE O JSON E RETORNA PARA O SEND
    if(strcmp(obj["method"].asCString(),"get_temperatura0") == 0){
        //printf("     [#] Sensor temperatura 0 solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_temperatura0(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        //printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    // STATUS DO MINSKY
    if(strcmp(obj["method"].asCString(),"arduino_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_arduino_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        //printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"battery_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_battery_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"upboard_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_upboard_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"motorg_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_motorg_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"motore_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_motore_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"motortd_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_motortd_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"motorte_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_motorte_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"motorfd_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_motorfd_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"motorfe_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_motorfe_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"realsense_status") == 0){
        printf("     [#] RealSense status solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),RealCamera.get_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    // Robot_SENSORS 1
    if(strcmp(obj["method"].asCString(),"robot_sensor1") == 0){
        printf("     [#] Sensor 1 solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_distancia1(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"robot_sensor2") == 0){
        printf("     [#] Sensor 1 solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_distancia2(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"robot_sensor3") == 0){
        printf("     [#] Sensor 1 solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_distancia3(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"robot_sensor4") == 0){
        printf("     [#] Sensor 1 solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_distancia4(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"robot_sensor5") == 0){
        printf("     [#] Sensor 1 solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_distancia5(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"robot_sensor6") == 0){
        printf("     [#] Sensor 1 solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_distancia6(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"robot_sensor7") == 0){
        printf("     [#] Sensor 1 solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_distancia7(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"robot_sensor8") == 0){
        printf("     [#] Sensor 1 solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_distancia8(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }

    if(strcmp(obj["method"].asCString(),"robot_cor1") == 0){
        printf("     [#] Sensor de cor1 solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_cor1(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"robot_cor2") == 0){
        printf("     [#] Sensor de cor2 solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_cor2(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }    
    if(strcmp(obj["method"].asCString(),"robot_marcha") == 0){
        printf("     [#] marcha solicitado \n");
        sprintf(json_data,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\naccess-control-allow-origin: *\nContent-Type: application/json; charset=UTF-8\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",server_agent,obj["method"].asCString(),objMinsky.get_marcha(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    // -------------------------------

    // --------------------------------------------------------------------

    // Caso não encontre a função solicitada
    sprintf(json_data,"Connection: close\n\n{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"Not Found\",\"id\":%d}",obj["method"].asCString(),obj["id"].asInt());
    pjson_data = (char*)malloc(strlen(json_data));
    memcpy(pjson_data,&json_data[0],strlen(json_data));
    *tam_out = strlen(json_data);
    printf("        [#] JSON-RESPOSTA not found retornado\n");
    return pjson_data;
    // --------------------------------------
}
bool cameras(){
    // Função responsavel por procurar cameras e inicializar a RealSense
    /* rs::context ctx;
    rs::device * dev;
    char * nome = NULL;
    for(int x = 0; x < 10;x++){
        dev = ctx.get_device(x);
        nome = (char*)dev->get_name();
        printf("      Index: %d \n",x);
        printf("      Nome: %s \n",nome);
        printf("      Porta: %s \n",dev->get_serial());
        printf("      Firmware: %s \n",dev->get_firmware_version());
        if(strcmp(camera_nome,nome) == 0){
            if(RealCamera.start(x)){
                return true;    
            }else{
                printf("      [F] Falha em abrir RealSense \n");
                return false;
            }
        }
        if(dev != NULL){
            break;
        }
    }
    printf("      [F] Nenhuma camera encontrada \n");
    return false;
    */
}


char * tratar_streaming(int socket,char * buffer,size_t tam,size_t * tam_out){
    char * buffer_saida = NULL;
    char header[256];
    char frame_header[256];
    char * frame = NULL;
    int tamanho_frame = 0;
    char * no_signal = NULL;
    int tamanho = 0,index=0;
    sprintf(header,"HTTP/1.0 200 OK\nPragma: no-cache\nServer: %s\nDate: Mon, 21 May 2018 14:05:56 GMT\nContent-Type: multipart/x-mixed-replace;boundary=--frame--\n\n",server_agent);
    //sprintf(frame_header,"--frame%d\nContent-length: 999999\nContent-type: image/jpeg\n\n",Camera_Atual);
    buffer_saida = (char*)malloc(strlen(header)+strlen(frame_header)+650000000);
    if(RealCamera.start_camera(Camera_Atual) == NULL){
        no_signal = carregar_arquivo("www/images/no_signal.jpg",tam_out);
        return no_signal;
    };
    memcpy(buffer_saida,header,strlen(header));
    memcpy(&buffer_saida[strlen(header)],frame_header,strlen(frame_header));
    index = strlen(header);
    printf("Gravando... \n");
    
    while(RealCamera.get_status()){
        frame = RealCamera.get_frame(&tamanho_frame);
        if(frame == NULL){
            no_signal = carregar_arquivo("www/images/no_signal.jpg",tam_out);
            return no_signal;
        }
        if(index > 0 && frame != NULL){
            sprintf(frame_header,"--frame--\nContent-length: %d\nContent-type: image/jpeg\n\n",tamanho_frame);
            tamanho = tamanho_frame + strlen(frame_header)+index;
            
            memcpy(&buffer_saida[index],frame_header,strlen(frame_header));
            memcpy(&buffer_saida[strlen(frame_header)+index],frame,tamanho_frame);
        }
        
        if(index == 0 && frame != NULL){
            sprintf(frame_header,"--frame--\nContent-length: %d\nContent-type: image/jpeg\n\n",tamanho_frame);
            tamanho = tamanho_frame+strlen(frame_header);
            memcpy(&buffer_saida[0],frame_header,strlen(frame_header));
            memcpy(&buffer_saida[strlen(frame_header)],frame,tamanho_frame);
           
        }
        if(send(socket,buffer_saida,tamanho,0)==-1){
            printf("[F] Falha em enviar o pacote de streaming\n");
            printf("Buffer: %p \nTamanho: %d \n",buffer_saida,tamanho);
            *tam_out = tamanho;
        }
        //memcpy(&buffer_saida[index],&frame_header,strlen(frame_header));
        index=0;
        memset(buffer_saida,0,tamanho);
        if(frame != NULL){
            free(frame);
        }
        *tam_out = tamanho;
    }                                                           
}
char * tratar_imagen(char * buffer,size_t tam,size_t * tam_out){    
	char triagem[1024] = "\0";
	char header[1024] = "\0";   
    char nomeFinal[25] = "\0";
	char * fbuffer = NULL,*xbuffer=NULL,*pointer=NULL;
	printf("	[Ok] Triagem de     imagen \n");
	strncpy(triagem,&buffer[12],25);
	pointer = strtok(triagem," ");
    if(pointer == NULL){
		printf("	[F] Falha em capturar o nome de arquivo \n");
		return pointer; 
	}
	
	sprintf(header,"HTTP/1.1 200 OK\nServer: %s\nConnection: Keep-Alive\nContent-Type: image/html; charset=UTF-8\n\n",server_agent);
	sprintf(nomeFinal,"www/images/%s",pointer);
    printf("		Recuperando arquivo: %s \n",nomeFinal);
	
    fbuffer = carregar_arquivo(nomeFinal,tam_out); 
    if(fbuffer == NULL){
        fbuffer = carregar_arquivo("www/images/404.png",tam_out); 
    }
	xbuffer = (char*)malloc(strlen(header)+*tam_out);
	memcpy(xbuffer,&header,strlen(header));
	memcpy(&xbuffer[strlen(header)],fbuffer,*tam_out);
	*tam_out += strlen(header);
	return xbuffer;
}

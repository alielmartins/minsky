#include <modulovideo.h>
#include <librealsense/rs.hpp>

bool realsense::start(int id){
    AID = id;
    dev = ctx.get_device(AID);
    if(dev == NULL){
        status = false;
        return false;
    }
    dev->enable_stream(rs::stream::color, 1920, 1080, rs::format::rgb8, 30); // Modo de Captura
    dev->start();
    status = true;
    return true;
}
bool realsense::stop(){
    dev->stop();
}
char * realsense::get_frame(){
    char * tela;
    dev->wait_for_frames();
    tela = (char *)dev->get_frame_data(rs::stream::color); // Seleciona camera
    return tela;
}

int realsense::get_status(){
    return 1;
}
#include <librealsense/rs.hpp>

class realsense{
    private:
        bool status = false;
        int AID;
        rs::context ctx;
        rs::device * dev;
        
    public:
        bool start(int id);
        bool stop();
        int get_status();
        char * get_frame();
    
};
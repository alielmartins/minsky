#ifndef MINSKY_H_INCLUDED
#define MINSKY_H_INCLUDED

class Minsky{
private:

    int flag_on=false;
    int temperatura0=255;
    int distancia0=255;
    int distancia1=255;
    int distancia2=255;
    int distancia3=255;
    int distancia4=255;
    int distancia5=255;
    int distancia6=255;
    int distancia7=255;
    int distancia8=255;

    int color1 = 255;
    int color2 = 255;
    int marcha=255;
    
    int battery=1;
    int upboard=1;
public:
    void home();
    bool starter(int auth);
    int get_sensors(int id_port);
    // STATUS GETS
    int get_arduino_status();
    int get_battery_status();
    int get_upboard_status();
    int get_motorg_status();
    int get_motore_status();
    int get_motorfd_status();
    int get_motorfe_status();
    int get_motortd_status();
    int get_motorte_status();
    // --------------
    
    int get_temperatura0();
    
    int get_distancia0();
    int get_distancia1();
    int get_distancia2();
    int get_distancia3();
    int get_distancia4();
    int get_distancia5();
    int get_distancia6();
    int get_distancia7();
    int get_distancia8();
    int get_cor1();
    int get_cor2();
    int get_marcha();
    int wait_key(int caracter);
    int def_var(char * texto);

};


#endif // MINSKY_H_INCLUDED

#ifndef MODULOVIDEO_H_INCLUDED
#define MODULOVIDEO_H_INCLUDED

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;

class Streaming{
    VideoCapture camera;
    Mat frame;
public:
  bool start_camera(int index);
  void stop_camera();
  char * get_frame(int * tamanho);
  int get_status();
  int get_statusValor();
};
#endif // MODULOVIDEO_H_INCLUDED

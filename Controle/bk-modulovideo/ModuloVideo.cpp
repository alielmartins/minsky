#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <ModuloVideo.h>
#include <vector>
#include <unistd.h>
using namespace cv;
using namespace std;

bool Streaming::start_camera(int index){
    return camera.open(index);
}
void Streaming::stop_camera(){
    camera.release();
}

vector <uchar>output;
char * Streaming::get_frame(int * tamanho){
    char * buffer = NULL;
    int tam=0;
    char fim[] = "\n\n";
    if(camera.read(frame) != true){
        printf("        [F] Nâo foi possivel ler o frame. \n");
        camera.release();
        return NULL;
    }
    output.resize(10000);
    imencode(".jpg",frame,output);
    tam = output.size();
    *tamanho = tam;
    buffer = (char*)malloc(tam);
    if(buffer == NULL){
        printf("Não foi possiveo allocar espaço \n");
        return NULL;
    }
    memcpy(buffer,&output[0],tam);
    return buffer;
}

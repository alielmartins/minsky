#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <string.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <Minsky.h>
#include <serialport.h>


void Minsky::home(){
    return;
}

bool Minsky::starter(int auth){
    char dev_name[25] = "\0";
    int id_port=0;
    printf("    [Ok]Conectando-se ao Minsky \n");
    printf("        Procurando dispositivos \n");
    for(int x=0;x < 10;x++){
        sprintf(dev_name,"/dev/ttyUSB%d",x);
        serialport MinhaPorta(dev_name,false);
        if(MinhaPorta.get_arduinoflag()){
            //printf("Conectado \n");
            flag_on = MinhaPorta.get_arduinoflag();
            id_port =  MinhaPorta.get_portid();
            break;
        }else{
            //printf("Desconectado \n");
        }
    }
    if(flag_on){
        get_sensors(id_port);
    }
    return flag_on;
}

int Minsky::get_sensors(int id_port){
    int caracteres=0,bytes=0,contador=0;
    char sensor_string[256] = "\0";
    if(flag_on){
        printf("Capturando valores dos sensores na porta serial \n");
        while(true){
            fflush(stdin);
            bytes = read(id_port,(void*)&caracteres,1);
            if(bytes > 0){
                if(wait_key(caracteres)){          // Primeira barra detectada
                    while(true){                    // Começa gravar os bytes da serial
                        fflush(stdin);
                        caracteres = 0;
                        bytes = read(id_port,(void*)&caracteres,1);
                        if(bytes > 0){
                            if(wait_key(caracteres)){  // Segunda barra detectada
                                break;
                            }
                            sensor_string[contador] = (char)caracteres;
                           //printf("%d \n",caracteres);
                            contador++;
                        }
                        printf("#@FINAL@#");
                    }
                    def_var(sensor_string);  // Set a variavel do sensor recebido
                }
            }    
            contador=0;
            memset(&sensor_string,0,256);
            caracteres = 0;
            bytes = 0;
        }
    }
}

int Minsky::wait_key(int caracter){
    if(caracter == 124){ // | Na tabela ASCII
        return true;
    }
    return false;
}
int Minsky::def_var(char * texto){
    // Função responsavel por capturar os sensores e definir os valores das variaveis
    printf("Sensor: %s : Tamanho: %d\n",texto,strlen(texto));
    if(strncmp(texto,"T0:",3) == 0){
        printf("Sensor de Temperatura0 encontrado. \n");
        temperatura0 = atoi(&texto[3]);
        printf("Temperatura: %d \n",temperatura0);
    }
    if(strncmp(texto,"D0:",3) == 0){
        printf("Sensor de Distancia0 encontrado. \n");
        distancia0 = atoi(&texto[3]);
        printf("Distancia: %d \n",distancia0);
    }
    if(strncmp(texto,"B0:",3) == 0){
        printf("Status da bateria\n");
        battery = atoi(&texto[3]);
        printf("Bateria: %d \n",distancia0);
    }
}
// GETS SENSORES
int Minsky::get_temperatura0(){
    return temperatura0;
}
int Minsky::get_distancia0(){
    return distancia0;
}
// GETS STATUS
int Minsky::get_arduino_status(){
    return flag_on;
}
int Minsky::get_upboard_status(){
    return upboard;
}
int Minsky::get_motore_status(){
    return 0;
}
int Minsky::get_motorg_status(){
    return 0;
}
int Minsky::get_motorfd_status(){
    return 0;
}
int Minsky::get_motorfe_status(){
    return 0;
}
int Minsky::get_motortd_status(){
    return 0;
}
int Minsky::get_motorte_status(){
    return 0;
}
int Minsky::get_battery_status(){
    return battery;
}
// ---------------



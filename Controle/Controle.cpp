#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <iostream>
#include <ModuloVideo.h>
#include <Minsky.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <jsoncpp/json/json.h>

#define port 80
#define server_agent "Minsky WebService 3.0v"
#define pedido_size 25
#define mb 5000000
#define max_cameras 10
#define msg_max 256
#define modo_debugger 1
#define raiz_web "www"
#define pag_web "www//index.html"
#define auth_arduino true

struct clients_infos{                                                                   // ssstruct necessaria para passar informações para thread
    int socket_id;
    struct sockaddr_in settings;
};
// FUNÇÔES EXTRA
void log(char * txt);                                                                   // Função que escreve no arquivo de LOG
void show(char * txt,int modo);
char * readfile(char * name,size_t * zfile);

// FUNÇÔES DO SERVIDOR
void * Start_Server(void * params);                                                             // Função que inicializa o servidor na porta passada como (int)parametro
void * clients(void * params);                                                          // Função que trata o cliente
int firewall(void * infos);                                                             // Função que define se a conexão é valida
int verificar_pacote(char * buffer,size_t tam);
char * tratar_http_pacotes(char * buffer,size_t tam,size_t * tam_out);
char * tratar_jsonrpc(char * buffer,size_t tam,size_t * tam_out);
char * jsonrpc_res(char * buffer,size_t tam,size_t * tam_out);
char * tratar_streaming(int socket,char * buffer,size_t tam,size_t * tam_out);
// FUNÇÔES DE CONTROLE DE VIDEO
int scan_cameras();

Minsky objMinsky;

int main(int, char * params[]){
    pthread_t threads;

    printf("[*] Controle %s [*]\n",server_agent);
    printf("    [!] Inicializando Servidor... \n");
    pthread_create(&threads,NULL,&Start_Server,NULL);
    if(threads != 0){
        printf("        Start [OK]\n        IP: 0.0.0.0\n        Port: %d\n",port);
    }

    printf("    [!] Inicializando Modulo de Video... \n");
    printf("        Procurando cameras...\n");
    printf("        Cameras encontradas: %d\n",scan_cameras());
    printf("    [!] Inicializando Joystick... \n");
    printf("        Joystick: Not Found \n");
    objMinsky.starter(auth_arduino);
    while(1){
        sleep(1);
    }
}

void * Start_Server(void * params){
    int Socket = 0,connection=0,clients_socket=0,id_con=0;
    char msg[msg_max] = "\0";
    pthread_t threads=0;
    socklen_t clients_size=0;
    struct sockaddr_in Settings;                                                        // Struct com configurações do servidor
    struct sockaddr_in clients_settings;                                                // Struct para receber informações do cliente
    struct clients_infos infos_clients;                                                 // Struct para passar informações do cliente
    memset(&Settings,0,sizeof(Settings));                                               // Limpa a struct com configurações do servidor
    memset(&clients_settings,0,sizeof(clients_settings));                               // Limpa a struct com configurações do cliente
    memset(&infos_clients,0,sizeof(infos_clients));                                     // Limpa a struct com informações do cliente
    dnv:Socket = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);                                   // Define regras da conexão
    if(Socket == -1){                                                                   // Verifica se o socket foi definido com sucesso
        show((char*)"[F] Falha em definir as configurações de  socket. \n",modo_debugger);
        return 0;
    }
    show((char*)"        [SOCKET] Socket definido com sucesso. \n",modo_debugger);                             // Adiciona no arquivo de log
    Settings.sin_family = AF_INET;                                                      // Define a familia do servidor
    Settings.sin_addr.s_addr = INADDR_ANY;                                              // Inicializa o servidor no IP principal da maquina
    Settings.sin_port = htons(port);                                                      // Define a porta a ser utilizada
    if( (connection = bind(Socket,(struct sockaddr*)&Settings,sizeof(Settings)))==-1){  // Inicializa o bind e verifica se foi realizado com sucesso
        show((char*)"        [F] Falha em inicializar o bind. ( Aguarde 1 minuto!! )\n",modo_debugger);
        sleep(1);
        goto dnv;
        return 0;
    }
    show((char*)"        [SOCKET] Bind inicializado com sucesso\n",modo_debugger);                            // Adiciona no arquivo de log
    listen(Socket,255);                                                                 // Quantidade de conexões suportadas pelo Socket
    show((char*)"        [Ok] Aguardando Conexão\n",modo_debugger);
    clients_size = sizeof(struct sockaddr_in);
    while(1){
        clients_socket = accept(Socket,(struct sockaddr*)&clients_settings,&clients_size);      // Estabelece conexão com cliente
        if(clients_socket == -1){
            show((char*)"[F] Falha em estabelecer conexão com cliente \n",modo_debugger);
        }
        if(firewall(&infos_clients)==1){                                                // Regras de conexão
            infos_clients.socket_id = clients_socket;                                   // Passa valor do socket do cliente para infos_clients
            memcpy(&infos_clients.settings,&clients_settings,sizeof(clients_settings)); // Passa configurações  (IP,PORTA,FAMILIA) para infos_clients
            sprintf(msg,"[Ok] #%d Cliente %s:%d conectado com o servidor. \n",id_con,inet_ntoa(clients_settings.sin_addr),port);
            show(msg,modo_debugger);
            pthread_create(&threads,NULL,&clients,&infos_clients);                      // Cria um processo para cada cliente
        }else{
            sprintf(msg,"[F] #%d Este cliente não esta autorizado a conectar ao servidor \n",id_con);
            show(msg,modo_debugger);
        }
        id_con++;
    }
    close(Socket);                                                                      // fecha o socket
}

void * clients(void * params){                                                          // Função que executa funções para o cliente
    char * buffer = NULL,*buffer_out=NULL,*tmp=NULL;
    char msg[msg_max] = "\0";
    int nbytes=0,v_pacote=0;
    size_t zbuffer_out=0;
    struct clients_infos infos_clients;
    memcpy(&infos_clients,params,sizeof(infos_clients));                                // Recupera informações do cliente
    buffer = (char*)malloc(mb);
    if((nbytes = recv(infos_clients.socket_id,buffer,mb,0))==-1){
        log((char*)"    [F] Falha em receber pacote do cliente\n");
    }else{
       sprintf(msg,(char*)"    [*] Pacote recebido pelo cliente \n        Bytes: %d\n",nbytes);
       show(msg,modo_debugger);
        if((v_pacote = verificar_pacote(buffer,nbytes)) > 0){
            if(v_pacote == 1){                                                           // Paginas HTTP
                buffer_out = tratar_http_pacotes(buffer,nbytes,&zbuffer_out);
            }
            if(v_pacote == 2){                                                           // Streaming
                log((char*)"Tratar Streaming \n");
                tmp = tratar_streaming(infos_clients.socket_id,buffer,nbytes,&zbuffer_out);
            }
            if(v_pacote == 3){                                                           // Pacotes JSON
                log((char*)"Tratar JSONRPC \n");
                tmp = tratar_jsonrpc(buffer,nbytes,&zbuffer_out);
                buffer_out = jsonrpc_res(tmp,zbuffer_out,&zbuffer_out);
            }
            if(buffer_out != NULL){
                if((nbytes=send(infos_clients.socket_id,buffer_out,zbuffer_out,0))==-1){
                    show((char*)"    [F] Falha em enviar pacote \n        Perda de conexão\n",modo_debugger);
                }
                show((char*)"    [OK] Resposta do servidor enviada para o cliente\n",modo_debugger);
            }
        }else{
            show((char*)"    [F] Pacote não permitido pelo servidor \n",modo_debugger);
        }
    }
    sprintf(msg,"    [*]Conexão com %s:%d finalizada\n\n",inet_ntoa(infos_clients.settings.sin_addr),port);
    show(msg,modo_debugger);
    free(buffer);
    free(tmp);
    free(buffer_out);
    close(infos_clients.socket_id);
    return NULL;
}
int verificar_pacote(char * buffer,size_t tam){
    char msg[msg_max] = "\0";
    show((char*)"    [Ok] Verificação de pacote\n",modo_debugger);
    char pedido[pedido_size] = "\0",*r_pedido;
    if(strncmp(buffer,"GET /",5)==0){
        strncpy(pedido,&buffer[4],25);
        r_pedido = strtok(pedido," ");
        sprintf(msg,(char*)"        Pacote: HTTP-GET\n        Pedido: %s\n",r_pedido);
        show(msg,modo_debugger);
        if(strncmp(pedido,"/streaming",10)==0){
            return 2;
        }
        return 1;
    }
    if(strncmp(buffer,"POST /",6) == 0){
        strncpy(pedido,&buffer[5],25);
        r_pedido = strtok(pedido," ");
        sprintf(msg,(char*)"        Pacote: HTTP-POST\n        Pedido: %s\n",pedido);
        show(msg,modo_debugger);
        if(strncmp(pedido,"/jsonrpc",8)==0){
            return 3;
        }
    }
    return 0;
}
char * tratar_http_pacotes(char * buffer,size_t tam,size_t * tam_out){
    char * buffer_out = NULL;
    char * buffer_file = NULL;
    char pedido[pedido_size] = "\0";
    char pagina[25] = "\0";
    char cabecalho[256] = "\0";
    size_t zfile = 0;
    strncpy(pagina,&buffer[4],25);
    strtok(pagina," ");
    sprintf(pedido,"%s%s",raiz_web,pagina);
    show((char*)"    [Ok] Tratamento de pacote HTTP-GET \n",modo_debugger);
    printf("        Pagina solicitada: %s \n",pedido);
    if(strcmp(pagina,"/")==0){
        buffer_file = readfile((char*)"www/index.html",&zfile);
    }else{
        buffer_file = readfile(pedido,&zfile);
    }
    if(buffer_file == NULL){
        printf("        Retornado 404 \n");
        buffer_file = readfile((char*)"www/404.html",&zfile);
    }
    sprintf(cabecalho,"HTTP/1.1 200 OK\nDate: Mon, 23 May 2005 22:38:34 GMT\nServer: %s\nLast-Modified: Wed, 08 Jan 2003 23:11:55 GMT\nContent-Length: %d\nConnection: close\nContent-Type: text/html; charset=UTF-8\n\n",(const char*)server_agent,(int)zfile);
    buffer_out = (char*)malloc(zfile+strlen(cabecalho));

    memset(buffer_out,0,zfile+strlen(cabecalho));
    if(buffer_out == NULL){
        show((char*)"    [*] Falha na allocação de memoria \nbuffer_out: NULL\n",modo_debugger);
    }
    memcpy(&buffer_out[0],&cabecalho[0],strlen(cabecalho));
    memcpy(&buffer_out[strlen(cabecalho)],buffer_file,zfile);

    *tam_out = strlen(cabecalho)+zfile;
    free(buffer_file);
    return buffer_out;
}
char * tratar_jsonrpc(char * buffer,size_t tam,size_t * tam_out){
    printf("    [Ok] Tratamento de JSONRPC \n");
    // Extrair o JSONRPC do HTTP e retorna um ponteiro para tal
    char * buffer_cpy = NULL,*jsonrpc_data=NULL,*jsonrpc_zdata=NULL,*tmp=NULL;
    char njson[256] = "\0";
    int zjson = 0;
    buffer_cpy = (char*)malloc(tam);
    memset(buffer_cpy,0,tam);
    memcpy(buffer_cpy,buffer,tam);
    jsonrpc_zdata = strstr(buffer_cpy,"Content-Length:");
    jsonrpc_zdata = strtok(jsonrpc_zdata,"\n");
    zjson = atoi(&jsonrpc_zdata[16]);
    // -------------------------
    tmp = strstr(buffer,"{");
    jsonrpc_data = (char*)malloc(zjson);
    memcpy(jsonrpc_data,tmp,zjson);
    Json::Reader reader;
    Json::Value obj;
    reader.parse(jsonrpc_data,obj);
    printf("    Versão: %s\n",obj["JSONRPC"].asCString());
    printf("    Method: %s\n",obj["method"].asCString());
    printf("    Params: %s\n",obj["params"].asCString());
    printf("    ID: %d\n",obj["ID"].asInt());
    free(buffer_cpy);
    *tam_out = zjson;
    // ---------------------------------------------------------
    return jsonrpc_data;
}
char * tratar_streaming(int socket,char * buffer,size_t tam,size_t * tam_out){
    char p_streaming[max_cameras+20] = "\0";
    char header[msg_max] = "\0";
    char frame_header[100] = "\0";
    char teste[] = "FIM\n\n";
    char * buffer_frame = NULL,*frame_buffer=NULL,*tmp,*no_signal=NULL;

    int index_camera=0,streaming_flags=1,zframe=0,nbytes=0,tamanho_total=0,zno_signal=0;


    printf("    [Ok] Tratamento de Streaming \n");
    memset(&p_streaming,0,max_cameras+20);
    strncpy(p_streaming,&buffer[5],max_cameras);
    printf("        Pedido Streaming: %s \n",p_streaming);
    index_camera = atoi(&p_streaming[9]);
    printf("        Camera: %d \n",index_camera);
    sprintf(header,"HTTP/1.0 200 OK\nContent-Type: multipart/x-mixed-replace; boundary=frame%d\nConnection: keep-alive\nServer: %s\nDate: Mon, 21 May 2018 14:05:56 GMT\n\n",index_camera,server_agent);
    sprintf(frame_header,"--frame%d\nContent-Type: image/jpeg\n\n",index_camera);
    buffer_frame = (char*)malloc(strlen(header)+mb);
    frame_buffer = (char*)malloc(mb+strlen(frame_header));
    memset(buffer_frame,0,strlen(header)+mb);
    memset(frame_buffer,0,mb+strlen(frame_header));
    // Montagem do primeiro pacote
    Streaming streaming_x;
    if(streaming_x.start_camera(index_camera)){
        printf("        Camera %d inicializada com sucesso. \n",index_camera);
        printf("        Streaming inicializado \n");
        while(true){
                tmp = streaming_x.get_frame(&zframe);
                // Monta o FRAME_BUFFER
                memcpy(&frame_buffer[0],&frame_header[0],strlen(frame_header));
                memcpy(&frame_buffer[strlen(frame_header)],tmp,zframe);
                // --------------------
                tamanho_total = strlen(frame_header)+zframe;
            if(streaming_flags == 1){
                memcpy(&buffer_frame[0],&header,strlen(header));
                memcpy(&buffer_frame[strlen(header)],frame_buffer,zframe+strlen(header)+strlen(frame_header));
                memcpy(&buffer_frame[strlen(header)+zframe+strlen(frame_header)],&teste[0],strlen(teste));
                tamanho_total = strlen(header)+zframe+strlen(frame_header)+strlen(teste);
                streaming_flags = 0;
            }else{
                memcpy(&buffer_frame[0],&frame_buffer[0],strlen(frame_header)+zframe);
                memcpy(&buffer_frame[zframe+strlen(frame_header)],&teste[0],strlen(teste));
                tamanho_total = strlen(frame_header)+zframe+strlen(teste);
            }
            if((nbytes = send(socket,buffer_frame,tamanho_total,0)) <= 0){
                printf("        [F] Falha em realizar o streaming \n");
                break;
            }
            memset(&frame_buffer[0],0,strlen(frame_header)+mb);
            memset(&buffer_frame[0],0,mb+strlen(header));
            free(tmp);
        }
        streaming_x.stop_camera();
        printf("        [*] Streaming finalizado com sucesso. \n");
    }else{
        printf("        [F] Não foi possivel inicializar a camera \n");
        no_signal = readfile((char*)"./www/images/no_signal.jpg",(size_t*)&zno_signal);
        memcpy(&buffer_frame[0],no_signal,zno_signal);
        if(send(socket,buffer_frame,strlen(header)+strlen(frame_header)+zno_signal,0)==-1){
            printf("    [F] Falha em enviar o no_signal \n");
            return NULL;
        }else{
            printf("    [Ok] no_signal enviado com sucesso. \n");
        }
        free(no_signal);
        // -------------------------------
    }
    free(buffer_frame);
    free(frame_buffer);
    return NULL;
}
char * jsonrpc_res(char * buffer,size_t tam,size_t * tam_out){
    char json_data[256]= "\0";
    char * pjson_data = NULL;
    Json::Reader reader;
    Json::Value obj;
    //printf("    [#] Tratamento de JSON--JSONRES\n");
    //printf("     [#] Conteudo: %s \n",buffer);
    //printf("     [#] Tamanho: %d \n",tam);
    reader.parse(buffer,obj);
    //printf("      [#] ID-RES: %d \n",obj["id"].asInt());
    //printf("      [#] Meth-RES: %s \n",obj["params"].asCString());
    // RECONHECE O PEDIDO DE SENSOR, REESCREVE O JSON E RETORNA PARA O SEND
    if(strcmp(obj["method"].asCString(),"get_temperatura0") == 0){
        //printf("     [#] Sensor temperatura 0 solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_temperatura0(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        //printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    // STATUS DO MINSKY
    if(strcmp(obj["method"].asCString(),"arduino_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_arduino_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        //printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"battery_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_battery_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"upboard_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_upboard_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"motorg_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_motorg_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"motore_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_motore_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"motortd_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_motortd_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"motorte_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_motorte_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"motorfd_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_motorfd_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"motorfe_status") == 0){
        printf("     [#] Arduino Status solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_motorfe_status(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    // Robot_SENSORS 1
    if(strcmp(obj["method"].asCString(),"robot_sensor1") == 0){
        printf("     [#] Sensor 1 solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_temperatura0(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"robot_sensor2") == 0){
        printf("     [#] Sensor 1 solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_distancia0(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"robot_sensor3") == 0){
        printf("     [#] Sensor 1 solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_temperatura0(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"robot_sensor4") == 0){
        printf("     [#] Sensor 1 solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_temperatura0(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"robot_sensor5") == 0){
        printf("     [#] Sensor 1 solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_temperatura0(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"robot_sensor6") == 0){
        printf("     [#] Sensor 1 solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_temperatura0(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"robot_sensor7") == 0){
        printf("     [#] Sensor 1 solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_temperatura0(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    if(strcmp(obj["method"].asCString(),"robot_sensor8") == 0){
        printf("     [#] Sensor 1 solicitado \n");
        sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"%d\",\"id\":%d}",obj["method"].asCString(),objMinsky.get_temperatura0(),obj["id"].asInt());
        pjson_data = (char*)malloc(strlen(json_data));
        memcpy(pjson_data,&json_data[0],strlen(json_data));
        *tam_out = strlen(json_data);
        printf("     [#] JSON-RESPOSTA retornado\n");
        return pjson_data;
    }
    // -------------------------------

    // --------------------------------------------------------------------

    // Caso não encontre a função solicitada
    sprintf(json_data,"{\"JSONRPC\":\"2.0\",\"method\":\"%s\",\"params\":\"Not Found\",\"id\":%d}",obj["method"].asCString(),obj["id"].asInt());
    pjson_data = (char*)malloc(strlen(json_data));
    memcpy(pjson_data,&json_data[0],strlen(json_data));
    *tam_out = strlen(json_data);
    printf("        [#] JSON-RESPOSTA not found retornado\n");
    return pjson_data;
    // --------------------------------------
}
int firewall(void * infos){                                                             // Autoriza ou não a conexão do cliente no servidor
    return 1;                                                                           // Retorna (1- Autorizado, 0-Não Autorizado)
}

// FUNÇÔES DE CONTROLE DE CAMERA
int scan_cameras(){
    int qnt=5;
    return qnt;
}
// FUNÇÔES ALEATORIAS
void log(char * txt){
    /* FILE * wLogs;                                                                       // Aponta para endereço do arquivo
    wLogs = fopen("logs.txt","a");                                                      // Abre o arquivo logs.txt caso não exista seja criado
    if(wLogs == NULL){                                                                  // Verifica se criou ou abrio com sucesso
        show((char*)"       [F] Falha em abrir o arquivo de log. \n",modo_debugger);
        return;
    }
    fprintf(wLogs,"%s",txt);                                                            // escreve os dados no arquivod
    fclose(wLogs);
    */
}
char * readfile(char * name,size_t * zfile){
    FILE * wFile = NULL;
    int nbytes=0;
    char * buffer = NULL;
    wFile = fopen(name,"rb");
    if(wFile == NULL){
        printf("        [F] Falha em abrir arquivo. \n");
        return NULL;
    }
    fseek(wFile,0,SEEK_END);
    *zfile = ftell(wFile);
    rewind(wFile);

    buffer = (char*)malloc(*zfile);
    nbytes = fread(&buffer[0],*zfile,1,wFile);
    if(nbytes < 0){
        printf("    [F] Falha na leitura dos bytes do arquivo solicitado. \n");
        fclose(wFile);
        return NULL;
    }
    fclose(wFile);
    return buffer;
}
void show(char * txt,int modo){
    if(modo == 1){
        printf("%s",txt);
        log(txt);
    }
    log(txt);
}


#ifndef MODULOVIDEO_H_INCLUDED
#define MODULOVIDEO_H_INCLUDED

class Streaming{
  
public:
  bool start_camera(int index);
  void stop_camera();
  char * get_frame(int * tamanho);
};
#endif // MODULOVIDEO_H_INCLUDED
